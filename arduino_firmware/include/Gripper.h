/*
    Gripper.h - Is a Class to test the 3EA-Gripper PCB for the Convectro Drone
    Author: Marvin Hauke
    written: 08.06.2021
*/

#ifndef Gripper_h
#define Gripper_h

#include <Arduino.h>
//#include <Servo.h>
#include <PID_v1.h>

class Gripper
{
public:
	//Member:
	//Variables:
	//Average min und max alues for the motorcalibration
	int m1_avrg_min = 0, m1_avrg_max = 0;
	int m2_avrg_min = 0, m2_avrg_max = 0;

	//Constructor:
	Gripper(int nfault_pin,
			int sensor_pin,
			int m1_wiper_pin,
			int m2_wiper_pin,
			int nsleep_pin,
			int m1_pin1,
			int m1_pin2,
			int m2_pin1,
			int m2_pin2);
	//Functions:
	void begin(Gripper gripper, bool run, int baudRate = 9600);
	void getDriverstatus();
	int getSensorstatus();
	void getGripperstatus();
	void calibrate(int calibration_runs);
	int getM1Pos();
	int getM2Pos();
	void open();
	void forwardFastDecay(); //use Fast Decay for current flow through the body Diodes
	void reverseFastDecay();
	void forwardSlowDecay(); //use slow Decay to shorten the Motorwindings.
	void reverseSlowDecay();
	void close();
	void pause();
	void sleep();
	void setDriverstate(bool on);
	void printMotorPos();

private:
	//Members:
	//pins:
	int _nfault_pin;
	int _nsleep_pin;
	int _sensor_pin;
	int _m1_wiper_pin;
	int _m2_wiper_pin;
	int _m1_pin1, _m1_pin2;
	int _m2_pin1, _m2_pin2;

	//values:
	const int _calibrationtime = 3000;
	bool _calibrationFlag = false;
	int _sensorvalue;
	int _m1_pos;
	int _m2_pos;

	//Functions:
	bool _isGripperOpen();
	bool _isGripperCalibrated();

	void _printCalibrationvalues();
	void _print(String arg1, int arg2);
	void _print(String arg1);
};

#endif