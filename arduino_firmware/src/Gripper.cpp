/*
    Gripper.h - Is a Class to test the 3EA-Gripper PCB for the Convectro Drone
    Author: Marvin Hauke
    written: 08.06.2021
*/

#include <Arduino.h>
#include <Gripper.h>
#include <PID_v1.h>
#include <Servo.h>

//************  Public area  ************
//Constructors:

//Default Constructor for Gripper
Gripper::Gripper(int nfault_pin,
                 int sensor_pin,
                 int m1_wiper_pin,
                 int m2_wiper_pin,
                 int nsleep_pin,
                 int a_in1_pin,
                 int a_in2_pin,
                 int b_in1_pin,
                 int b_in2_pin)
{
  //set the private members:
  _nfault_pin = nfault_pin;
  _sensor_pin = sensor_pin;
  _m1_wiper_pin = m1_wiper_pin;
  _m2_wiper_pin = m2_wiper_pin;
  _nsleep_pin = nsleep_pin;
  _m1_pin1 = a_in1_pin;
  _m1_pin2 = a_in2_pin;
  _m2_pin1 = b_in1_pin;
  _m2_pin2 = b_in2_pin;

  //atach Servo Pins
}

//Put this in the Setup() to start the Gripper
void Gripper::begin(Gripper gripper, bool on = false, int baudRate)
{
  Serial.begin(baudRate);
  pinMode(_nfault_pin, INPUT);
  pinMode(_sensor_pin, INPUT);
  pinMode(_m1_wiper_pin, INPUT);
  pinMode(_m2_wiper_pin, INPUT);
  pinMode(_nsleep_pin, OUTPUT);
  pinMode(_m1_pin1, OUTPUT);
  pinMode(_m1_pin2, OUTPUT);
  pinMode(_m2_pin1, OUTPUT);
  pinMode(_m2_pin2, OUTPUT);

  setDriverstate(on);
  _isGripperCalibrated();
  _isGripperOpen();
  _print("Gripper instatiated!");
}

//Returns the Driverstatus as a Serialoutput
void Gripper::getDriverstatus()
{
  digitalRead(_nfault_pin);
  if (_nfault_pin && _nsleep_pin)
  {
    _print("Driver is activ");
  }
  else if (!_nfault_pin)
  {
    _print("Driver failed!");
  }
  else if (!_nsleep_pin)
  {
    _print("Driver is sleeping");
  }
}

//Returns the status of the inductionsensor
int Gripper::getSensorstatus()
{
  return digitalRead(_sensor_pin);
}

//prints aktual Gripperdata
void Gripper::getGripperstatus()
{
  getDriverstatus();
  _printCalibrationvalues();
  printMotorPos();
  if (getSensorstatus() == HIGH)
  {
    _print("Payload attached");
  }
  else if (getSensorstatus() == LOW)
  {
    _print("No payload attached");
  }

  if (_isGripperOpen())
  {
    _print("The Gripper is open");
  }
  else if (!_isGripperOpen())
  {
    _print("The Gripper is closed");
  }
  if (!_isGripperCalibrated())
  {
    _print("Error: No valid Gripperstate. Please calibrate");
  }
}

void Gripper::setDriverstate(bool on)
{
  if (on)
  {
    digitalWrite(_nsleep_pin, HIGH);
  }
  else
  {
    digitalWrite(_nsleep_pin, LOW);
  }
}

int Gripper::getM1Pos()
{
  return analogRead(_m1_wiper_pin);
}

int Gripper::getM2Pos()
{
  return analogRead(_m2_wiper_pin);
}

void Gripper::calibrate(int calibration_runs)
{
  _print("Please wait while the Gripper is calibrating! Runs to complete the calibration: ", calibration_runs);
  int m1_sum_min, m1_sum_max;
  int m2_sum_min, m2_sum_max;

  //reset avrage values
  m1_avrg_min = 0;
  m1_avrg_max = 0;
  m2_avrg_min = 0;
  m2_avrg_max = 0;

  int pos = (getM1Pos() + getM2Pos()) / 2;
  _print("actual pos = ", pos);

  if (pos > 100 && pos < 700)
  {
    for (int i = 0; i < calibration_runs; i++)
    {
      m1_sum_max += getM1Pos();
      m2_sum_max += getM2Pos();
      open();
      m1_sum_min += getM1Pos();
      m2_sum_min += getM2Pos();
      close();
    }
    open(); //drives the Gripper into default position
  }
  else if (pos < 100)
  {
    for (int i = 0; i < calibration_runs; i++)
    {
      m1_sum_min += getM1Pos();
      m2_sum_min += getM2Pos();
      close();
      m1_sum_max += getM1Pos();
      m2_sum_max += getM2Pos();
      open();
    }
  }
  m1_avrg_min = m1_sum_min / calibration_runs;
  m1_avrg_max = m1_sum_max / calibration_runs;
  m2_avrg_min = m2_sum_min / calibration_runs;
  m2_avrg_max = m2_sum_max / calibration_runs;
  _print("Calibration finished!");
  _printCalibrationvalues();
  printMotorPos();
}

//Opens the Gripper
//TODO:implement PID
void Gripper::open()
{
  if (!_isGripperOpen())
  {

    analogWrite(_m1_pin1, 0);
    analogWrite(_m2_pin1, 0);
    analogWrite(_m1_pin2, 255);
    analogWrite(_m2_pin2, 255);
    delay(_calibrationtime);
    pause();
  }
  else
  {
    _print("Error: Gripper is allready open");
  }
}

//Closes the Gripper
//TODO:implement PID
void Gripper::close()
{
  if (_isGripperOpen())
  {
    analogWrite(_m1_pin1, 255);
    analogWrite(_m2_pin1, 255);
    analogWrite(_m1_pin2, 0);
    analogWrite(_m2_pin2, 0);
    delay(_calibrationtime);
    pause();
  }
  else
  {
    _print("Error: Gripper is allready closed");
  }
}

//Pauses the Gripper
void Gripper::pause()
{
  analogWrite(_m1_pin1, 0);
  analogWrite(_m2_pin1, 0);
  analogWrite(_m1_pin2, 0);
  analogWrite(_m2_pin2, 0);
}

void Gripper::printMotorPos()
{
  _print("actual M1 Pos: ", getM1Pos());
  _print("actual M2 Pos: ", getM2Pos());
}

void Gripper::forwardFastDecay()
{
  //xin_1 = PWM
  //xin_2 = 0
}

void Gripper::forwardSlowDecay()
{
  //xin_1 = 1
  //xin_2 = PWM
}

void Gripper::reverseFastDecay()
{
  //xin_1 = 0
  //xin_2 = PWM
}

void Gripper::reverseSlowDecay()
{
  //xin_1 = PWM
  //xin_2 = 1
}

//************  Private area  ************

//functions:

bool Gripper::_isGripperOpen()
{
  int pos = (getM1Pos() + getM2Pos() / 2);

  if (pos < 100)
  {
    return true;
  }
  else if (pos > 600) //aktuell auf 3,3V in Rev. 1 auf 1000 erhöhen
  {
    return false;
  }
}

bool Gripper::_isGripperCalibrated()
{
  int avrg_sum = m1_avrg_max + m1_avrg_min + m2_avrg_max + m2_avrg_min;

  if (avrg_sum != 0)
  {
    return true;
  }
  else
  {
    _print("The Gripper needs to be calibrated. Run 'gripper_calibrate <runs>'. At the moment the sum of all avragvalues = ", avrg_sum);
    return false;
  }
}

void Gripper::_printCalibrationvalues()
{
  _print("************ Calibrationvalues *************");
  _print("M1 avrg min = ", m1_avrg_min);
  _print("M2 avrg min = ", m2_avrg_min);
  _print("M1 avrg max = ", m1_avrg_max);
  _print("M2 avrg max = ", m2_avrg_max);
  _print("********************************************");
}

void Gripper::_print(String arg1, int arg2)
{
  Serial.print(arg1);
  Serial.print(arg2);
  Serial.print("\n");
}

void Gripper::_print(String arg1)
{
  Serial.println(arg1);
}
