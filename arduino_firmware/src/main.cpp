#include <Arduino.h>
#include <Gripper.h>
#include <cmdArduino.h> //TODO: include moddiffied cmdArduino.h to repo.

//TODO change baudrate to 115200
#define baudrate 9600

//Wiper and Sensorpins
#define sensor_pin A2
#define m1_wiper A0
#define m2_wiper A1
#define b_in1 10
#define b_in2 9
#define a_in1 6
#define a_in2 5
#define nsleep 2
#define nfault 3

Gripper gripper(nfault,
                sensor_pin,
                m1_wiper,
                m2_wiper,
                nsleep,
                a_in1,
                a_in2,
                b_in1,
                b_in2);

//Variables
double Setpoint;
double Input;
double Output;
double Kp = 0, Ki = 10, Kd = 0;

PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

//CMD Funktions deklaration:
void init_cmd();
void help(int argCnt, char **args);
void gripper_getStatus(int argCnt, char **args);
void gripper_getSensorState(int argCnt, char **args);
void gripper_getM1Pos(int argCnt, char **args);
void gripper_getM2Pos(int argCnt, char **args);
void gripper_printMotorPos(int argCnt, char **args);
void gripper_calibrate(int argCnt, char **args);
void gripper_open(int argCnt, char **args);
void gripper_close(int argCnt, char **args);

//PID regler implementieren zum verfahren der Motoren.
//aref auf 3,3V legen.(in REV1 auf 5V gelegt)

void setup()
{
  cmd.begin(baudrate);
  init_cmd();
  gripper.begin(gripper, true, baudrate);
  Serial.println("*************** CMD *******************");
  Serial.println("CMD >>");
}

void loop()
{
  cmd.poll();
}

void init_cmd()
{
  cmd.add("help", help);
  cmd.add("gripper_calibrate", gripper_calibrate);
  cmd.add("gripper_getStatus", gripper_getStatus);
  cmd.add("gripper_getSensorState", gripper_getSensorState);
  cmd.add("gripper_getM1Pos", gripper_getM1Pos);
  cmd.add("gripper_getM2Pos", gripper_getM2Pos);
  cmd.add("gripper_printMotorPos", gripper_printMotorPos);
  cmd.add("gripper_open", gripper_open);
  cmd.add("gripper_close", gripper_close);

  Serial.println("All Commands are intialized.");
  Serial.println("Use 'help' for a full commandlist");
  Serial.println("************************************************");
}

//CMD Funktions:

/** 
 * @brief Prints an overview of all available funktions.
 * 
 * @param argCnt 
 * @param args 
 */
void help(int argCnt, char **args)
{
  Serial.println(" Funktions:            <Parameters>     Description/returnvalues:");
  Serial.println("'help'                             -->  shows all available commands");
  Serial.println("'gripper_status'                   -->  prints the actual Motorposition and the DRV8833 Driverstate");
  Serial.println("'gripper_calibrate        <runs>'  -->  calibrates the gripper in <runs>");
  Serial.println("'gripper_getSensorState'           -->  returns SensorState in int");
  Serial.println("'gripper_getM1Pos'                 -->  returns M1Pos in int");
  Serial.println("'gripper_getM2Pos'                 -->  returns M2Pos in int");
  Serial.println("'gripper_printMotorPos'            -->  prints the actual Position of M1 and M2");
  Serial.println("'gripper_open'                     -->  opens the gripper");
  Serial.println("'gripper_close'                    -->  closes the gripper");
}

/** 
 * @brief Prints a Status of the gripper 
 * 
 * @param argCnt 
 * @param args 
 */
void gripper_getStatus(int argCnt, char **args)
{
  gripper.getGripperstatus();
}

void gripper_calibrate(int argCnt, char **args)
{
  int calibration_runs;
  if (argCnt > 1)
  {
    calibration_runs = cmd.conv(args[1], 10);
    if (calibration_runs <= 10)
    {
      gripper.calibrate(calibration_runs);
    }
    else
    {
      Serial.println("Error: not more then 10 calibrationruns allowed!");
    }
  }
  else
  {
    Serial.println("Error: gripper_calibrate has no valid number on calibrationruns!");
  }
}

void gripper_getSensorState(int argCnt, char **args)
{
  gripper.getSensorstatus();
}

void gripper_getM1Pos(int argCnt, char **args)
{
  gripper.getM1Pos();
}

void gripper_getM2Pos(int argCnt, char **args)
{
  gripper.getM2Pos();
}

void gripper_printMotorPos(int argCnt, char **args)
{
  gripper.printMotorPos();
}

//TODO: Error output if possition cant be reached.
//--> timeout 2Sec.
void gripper_open(int argCnt, char **args)
{
  gripper.open();
}

void gripper_close(int argCnt, char **args)
{
  gripper.close();
}
